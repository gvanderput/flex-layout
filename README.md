# flex-layout

A simple React component that allows you to quickly create Flebox CSS elements.

## Credits

Gerard van der Put, February 2021.  
https://gerardvanderput.com  
https://twitter.com/@GerardPut

## Installation:

```
$ yarn add @gerardvanderput/flex-layout
```

## Properties

The Flex component extends a regular DIV element.

| property | type    | required | default | description                                               |
| -------- | ------- | -------- | ------- | --------------------------------------------------------- |
| vertical | boolean | no       | false   | Will set `flex-direction` to **column**.                  |
| middle   | boolean | no       | false   | Render children aligned to the **middle** (vertically).   |
| bottom   | boolean | no       | false   | Render children aligned to the **bottom**.                |
| center   | boolean | no       | false   | Render children aligned to the **center** (horizontally). |
| right    | boolean | no       | false   | Render children aligned to the **right**.                 |

## Usage:

```
import Flex from '@gerardvanderput/flex-layout';

return (
    <Flex style={{ height: 200 }}>
        <Flex middle center>
            Hello
        </Flex>
        <Flex bottom right>
            World!
        </Flex>
    </Flex>
)
```
