import Flex from "../Flex/Flex";
import styles from "./example.module.scss";

export default function Example() {
  return (
    <>
      <Flex vertical style={{ marginBottom: 50, height: 200 }}>
        <Flex className={styles.name} right>
          <Flex className={styles.yellow}>John Doe</Flex>
          <Flex className={styles.green}>Suzanne Seria</Flex>
        </Flex>
        <Flex className={styles.age} middle center>
          50 years
        </Flex>
      </Flex>
      <Flex style={{ height: 100 }}>
        <Flex className={styles.name} top left>
          <Flex className={styles.yellow}>John Doe</Flex>
          <Flex className={styles.green} middle center>
            <div>Suzanne Seria</div>
          </Flex>
        </Flex>
        <Flex className={styles.age} middle left>
          50 years
        </Flex>
      </Flex>
    </>
  );
}
