import React from "react";
import styles from "./flex.module.scss";
import cn from "classnames";

interface FlexProps extends React.HTMLAttributes<HTMLDivElement> {
  width?: number;
  vertical?: boolean;
  left?: boolean;
  right?: boolean;
  center?: boolean;
  top?: boolean;
  bottom?: boolean;
  middle?: boolean;
}

const Flex: React.FC<FlexProps> = ({
  children,
  vertical = false,
  left = false,
  right = false,
  center = false,
  top = false,
  bottom = false,
  middle = false,
  ...props
}) => {
  return (
    <div
      {...props}
      className={cn(props.className, styles.flex, {
        [styles.horizontal]: !vertical,
        [styles.vertical]: vertical,
        [styles.right]: right,
        [styles.center]: !right && center,
        [styles.bottom]: bottom,
        [styles.middle]: !bottom && middle,
      })}
    >
      {children}
    </div>
  );
};

export default Flex;
