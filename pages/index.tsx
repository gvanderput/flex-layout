import Example from "../components/Example/Example";

export default function Home() {
  return <Example />;
}

export async function getStaticProps() {
  return {
    props: {},
  };
}
